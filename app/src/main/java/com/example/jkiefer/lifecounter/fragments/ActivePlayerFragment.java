package com.example.jkiefer.lifecounter.fragments;

import android.content.SharedPreferences;
import android.os.Bundle;
import android.os.Parcelable;
import android.support.v4.app.Fragment;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.TextView;

import com.example.jkiefer.lifecounter.R;
import com.example.jkiefer.lifecounter.models.Player;
import com.example.jkiefer.lifecounter.utils.CounterButtonEvent;
import com.google.gson.Gson;

import java.io.Serializable;

/**
 * Created by jkiefer on 7/1/15.
 */
public class ActivePlayerFragment extends Fragment{

    private static final String TAG = ActivePlayerFragment.class.getName();

    private TextView baseLevel, itemLevel, bonusLevel, totalLevel, playerName;
    private Button baseLevelUp, baseLevelDown;
    private Button itemLevelUp, itemLevelDown;
    private Button bonusLevelUp, bonusLevelDown;

    private Player player;

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState)
    {
        View view = inflater.inflate(R.layout.active_player_fragment, container, false);

        String playerJson = getArguments().getString("player");
        Gson gson = new Gson();
        player = gson.fromJson(playerJson, Player.class);

        baseLevel = (TextView) view.findViewById(R.id.base_level);
        itemLevel = (TextView) view.findViewById(R.id.item_level);
        bonusLevel = (TextView) view.findViewById(R.id.bonus_level);
        playerName = (TextView) view.findViewById(R.id.player_name);
        totalLevel = (TextView) view.findViewById(R.id.total_level);

        baseLevelUp = (Button) view.findViewById(R.id.base_level_up);
        baseLevelDown = (Button) view.findViewById(R.id.base_level_down);
        itemLevelUp = (Button) view.findViewById(R.id.item_level_up);
        itemLevelDown = (Button) view.findViewById(R.id.item_level_down);
        bonusLevelUp = (Button) view.findViewById(R.id.bonus_level_up);
        bonusLevelDown = (Button) view.findViewById(R.id.bonus_level_down);

        playerName.setText(player.getPlayerName());
        baseLevel.setText(String.valueOf(player.getBaseLevel()));
        itemLevel.setText(String.valueOf(player.getItemLevel()));
        bonusLevel.setText(String.valueOf(player.getBonusLevel()));
        totalLevel.setText(String.valueOf(player.getTotalLevel()));

        playerName.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

            }
        });

        baseLevelUp.setOnClickListener(new onLevelUpClickEvent(Player.BASELEVEL, 1));
        baseLevelDown.setOnClickListener(new onLevelUpClickEvent(Player.BASELEVEL, -1));
        itemLevelUp.setOnClickListener(new onLevelUpClickEvent(Player.ITEMLEVEL, 1));
        itemLevelDown.setOnClickListener(new onLevelUpClickEvent(Player.ITEMLEVEL, -1));
        bonusLevelUp.setOnClickListener(new onLevelUpClickEvent(Player.BONUSELEVEL, 1));
        bonusLevelDown.setOnClickListener(new onLevelUpClickEvent(Player.BONUSELEVEL, -1));

        return view;
    }

    private class onLevelUpClickEvent implements View.OnClickListener {
        private int mLevel;
        private int direction;

        public onLevelUpClickEvent(int type, int nDirection)
        {
            mLevel = type;
            direction = nDirection;
        }

        @Override
        public void onClick(View v) {
            Log.d(TAG, "clicking. itemToUpdate: " + mLevel);
            if(mLevel == Player.BASELEVEL)
            {
                player.setBaseLevel(direction);
            } else if (mLevel == Player.ITEMLEVEL) {
                player.setItemLevel(direction);
            } else if (mLevel == Player.BONUSELEVEL) {
                player.setBonusLevel(direction);
            } else {
                Log.e(TAG, "Unknown level type to update");
            }
            Log.d(TAG, "setting text: baseLevel: " + player.getBaseLevel());
            baseLevel.setText(String.valueOf(player.getBaseLevel()));
            itemLevel.setText(String.valueOf(player.getItemLevel()));
            bonusLevel.setText(String.valueOf(player.getBonusLevel()));
            totalLevel.setText(String.valueOf(player.getTotalLevel()));
        }
    }

    @Override
    public void onPause() {
        Log.d("ActivePlayerFragment", "saving player information");
        SharedPreferences sp = getActivity().getSharedPreferences("LifeCounter", 0);
        Gson gson = new Gson();
        sp.edit().putString("player"+player.getId(), gson.toJson(player)).commit();
        super.onPause();
    }
}
