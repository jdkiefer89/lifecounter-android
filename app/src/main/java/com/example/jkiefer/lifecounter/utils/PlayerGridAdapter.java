package com.example.jkiefer.lifecounter.utils;

import android.content.Context;
import android.content.SharedPreferences;
import android.os.Bundle;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentTransaction;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import com.example.jkiefer.lifecounter.MainActivity;
import com.example.jkiefer.lifecounter.R;
import com.example.jkiefer.lifecounter.fragments.ActivePlayerFragment;
import com.example.jkiefer.lifecounter.models.Player;
import com.google.gson.Gson;

import java.util.ArrayList;

/**
 * Created by jkiefer on 7/5/15.
 */
public class PlayerGridAdapter extends RecyclerView.Adapter<PlayerGridAdapter.ViewHolder> {

    public ArrayList<Player> playerDataset;
    public MainActivity ma;
    private Context mContext;
    private SharedPreferences sp;

    public static class ViewHolder extends RecyclerView.ViewHolder
        implements View.OnClickListener {
        public TextView playerName;
        public TextView playerLevel;
        private MainActivity mainActivity;
        private ArrayList<Player> mDataset;

        public ViewHolder(View v, MainActivity mActivity, ArrayList<Player> dataset) {
            super(v);
            v.setOnClickListener(this);
            playerName = (TextView) v.findViewById(R.id.player_name_grid);
            playerLevel = (TextView) v.findViewById(R.id.player_total_level);
            mDataset = dataset;
            mainActivity = mActivity;
        }

        @Override
        public void onClick(View v) {
            Log.d("ViewHolder", "View clicked. position: " + getPosition());

            ActivePlayerFragment playerFragment = new ActivePlayerFragment();
            Bundle player = new Bundle();
            Gson gson = new Gson();
            String playerJson = gson.toJson(mDataset.get(getPosition()));
            Log.d("PlayerGridApater", "player json: " + playerJson);
            player.putString("player", playerJson);
            playerFragment.setArguments(player);
            FragmentTransaction transaction = mainActivity.getSupportFragmentManager().beginTransaction();
            transaction.replace(R.id.fragment_layout, playerFragment);
            transaction.addToBackStack(null);
            transaction.commit();
        }
    }

    public void add(int position, Player p) {
        playerDataset.add(position, p);
        notifyItemInserted(position);

    }

    public void remove(Player p) {
        int position = playerDataset.indexOf(p);
        playerDataset.remove(position);
        notifyItemRemoved(position);
    }

    public PlayerGridAdapter(ArrayList<Player> nDataSet, MainActivity mainActivity, Context context) {
        playerDataset = nDataSet;
        mContext = context;
        ma = mainActivity;
        sp = mContext.getSharedPreferences("LifeCounter", 0);
    }

    @Override
    public ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View v = LayoutInflater.from(parent.getContext())
                                .inflate(R.layout.player_grid_item, parent, false);

        ViewHolder vh = new ViewHolder(v, ma, playerDataset);

        return vh;
    }

    @Override
    public void onBindViewHolder(ViewHolder holder, int position) {
        holder.playerLevel.setText(String.valueOf(playerDataset.get(position).getTotalLevelNoBonus()));
        holder.playerName.setText(playerDataset.get(position).getPlayerName());
    }

    @Override
    public int getItemCount() {
        return playerDataset.size();
    }
}
