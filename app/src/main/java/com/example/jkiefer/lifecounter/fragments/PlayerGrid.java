package com.example.jkiefer.lifecounter.fragments;

import android.content.SharedPreferences;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v7.widget.GridLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.text.TextUtils;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;

import com.example.jkiefer.lifecounter.MainActivity;
import com.example.jkiefer.lifecounter.R;
import com.example.jkiefer.lifecounter.models.Player;
import com.example.jkiefer.lifecounter.utils.PlayerGridAdapter;
import com.google.gson.Gson;

import java.util.ArrayList;

/**
 * Created by jkiefer on 7/2/15.
 */
public class PlayerGrid extends Fragment {

    private int newPlayerMenuItemId = 99;

    private RecyclerView mRecyclerView;
    private RecyclerView.Adapter mAdapter;
    private RecyclerView.LayoutManager mLayoutManager;
    private PlayerGridAdapter playerAdapter;
    public MainActivity mActivity;
    public SharedPreferences sp;
    public Gson gson;

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setHasOptionsMenu(true);
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState)
    {
        View view = inflater.inflate(R.layout.player_grid_fragment, container, false);

        sp = getActivity().getSharedPreferences("LifeCounter", 0);

        mRecyclerView = (RecyclerView) view.findViewById(R.id.player_recycler_view);

        mRecyclerView.setHasFixedSize(true);

        mLayoutManager = new GridLayoutManager(getActivity(), 3);

        mRecyclerView.setLayoutManager(mLayoutManager);

        ArrayList<Player> players = new ArrayList<Player>();
        int numberOfPlayers = sp.getInt("numberOfPlayers", 4);//default of 4 players
        gson = new Gson();
        for (int i=0; i<numberOfPlayers; i++) {
            //attempt to get saved players
            String savedPlayer = sp.getString("player"+i, null);
            Player player;
            if (TextUtils.isEmpty(savedPlayer)) {
                Log.d("PlayerGrid", "Add new player");
                player = new Player();
                player.setId(i);
                player.setPlayerName(String.format(getResources().getString(R.string.default_player_name), i + 1));
                sp.edit().putString("player" + i, gson.toJson(player)).commit();
            } else {
                Log.d("PlayerGrid", "getting existing player");

                player = gson.fromJson(savedPlayer, Player.class);
            }
            players.add(player);
        }

        playerAdapter = new PlayerGridAdapter(players, mActivity, getActivity());
        mAdapter = playerAdapter;
        mRecyclerView.setAdapter(mAdapter);

        return view;
    }

    @Override
    public void onResume() {
        mAdapter.notifyDataSetChanged();
        super.onResume();
    }

    public void addNewPlayer() {
        Player p = new Player();
        p.setPlayerName(String.format(getResources()
                .getString(R.string.default_player_name), mAdapter.getItemCount()+1));
        p.setId(mAdapter.getItemCount());
        playerAdapter.add(mAdapter.getItemCount(), p);
        sp.edit().putString("player"+mAdapter.getItemCount(), gson.toJson(p))
                .putInt("numberOfPlayers", mAdapter.getItemCount()).commit();
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        if(item.getItemId() == R.id.new_player) {
            Log.d("PlayerGrid", "Add a new player");
            addNewPlayer();
            return true;
        }
        return false;
    }
}
