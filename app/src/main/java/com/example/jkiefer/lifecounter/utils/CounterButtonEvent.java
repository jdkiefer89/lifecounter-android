package com.example.jkiefer.lifecounter.utils;

import android.content.Context;
import android.util.Log;
import android.view.View;
import android.widget.TextView;

import com.example.jkiefer.lifecounter.R;
import com.example.jkiefer.lifecounter.models.Player;

import java.text.NumberFormat;

/**
 * Created by jkiefer on 7/1/15.
 */
public class CounterButtonEvent implements View.OnClickListener {

    private final static String TAG = CounterButtonEvent.class.getSimpleName();

    private int itemToUpdate, direction;
    private Player mPlayer;

    public CounterButtonEvent(int itemToUpdate, int direction, Player player)
    {
        this.itemToUpdate = itemToUpdate;
        this.direction = direction;
        this.mPlayer = player;
    }

    @Override
    public void onClick(View v) {
        Log.d(TAG, "clicking. itemToUpdate: " + itemToUpdate);
        switch (itemToUpdate)
        {
            case Player.BASELEVEL:
                mPlayer.setBaseLevel(direction);
                break;
            case Player.ITEMLEVEL:
                mPlayer.setItemLevel(direction);
                break;
            case Player.BONUSELEVEL:
                mPlayer.setBonusLevel(direction);
                break;
        }
    }
}
