# Life Counter #

### A life counter application for you and your friends ###

  A simple application to keep track of you and your friends scores for Munchkin.

* This is currently a work in progress
* Supports Android devices 4.4+
* Built with Android Studio
* Designed with Material in mind

### Who do I talk to? ###

* Me, the repo owner and admin

### Legalz ###
This is just a helper application. I am in no way affiliated with Munchkin or Steve Jackson Games.