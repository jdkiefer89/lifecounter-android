package com.example.jkiefer.lifecounter.models;

import android.content.Context;
import android.util.Log;

import com.example.jkiefer.lifecounter.R;

import java.io.Serializable;

/**
 * Created by jkiefer on 7/1/15.
 */
public class Player {

    public static final int BASELEVEL = 1;
    public static final int ITEMLEVEL = 2;
    public static final int BONUSELEVEL = 3;

    private int baseLevel = 1;
    private int itemLevel = 0;
    private int bonusLevel = 0;
    private String playerName;
    private int id;
    private Context mContext;

    public int getId() {
        return id;
    }

    public void setId(int nId) {
        id = nId;
    }

    public int getBaseLevel () {
        Log.d("Player", "getting base level");
        return baseLevel;
    }

    public int getItemLevel () {
        return itemLevel;
    }

    public int getBonusLevel () {
        return bonusLevel;
    }

    public void setBaseLevel (int direction) {
        Log.d("Player", "updating base level: " + baseLevel);
        baseLevel += direction;
        if(baseLevel < 0) {
            baseLevel = 0;
        }
    }

    public void setItemLevel (int direction) {
        itemLevel += direction;
        if(itemLevel < 0) {
            itemLevel = 0;
        }
    }

    public void setBonusLevel (int direction) {
        bonusLevel += direction;
    }

    public String getPlayerName() {
        return playerName;
    }

    public void setPlayerName(String nPlayerName) {
        playerName = nPlayerName;
    }

    public int getTotalLevel() {
        return baseLevel + bonusLevel + itemLevel;
    }

    public int getTotalLevelNoBonus() { return baseLevel + itemLevel; }
}
